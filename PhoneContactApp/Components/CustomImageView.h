//
//  CustomImageView.h
//  Version 1.1
//

#import <UIKit/UIKit.h>

static const NSInteger kMaxCacheAge = 60 * 60 * 24 * 7; // 1 Week in Seconds
static const NSInteger kMaxDiskCountLimit = 1048576000; // 1GB in B

@class CustomImageView;

@protocol CustomImageViewDelegate <NSObject>

@optional

- (void)imageViewDidFinishLoadImage:(CustomImageView *)imageView;

@end

@interface CustomImageView : UIImageView

@property (weak, nonatomic) id<CustomImageViewDelegate> delegate;

@property (strong, nonatomic) NSString *imageURLString;

+ (void)saveImageToCache:(UIImage *)image withKey:(NSString *)key;
+ (void)imageFromCacheWithKey:(NSString *)key success:(void (^)(UIImage *savedImage))success; //Run in background thread, async
+ (UIImage *)imageFromCacheWithKey:(NSString *)key; //Run in main thread
+ (void)removeImageFromCacheWithKey:(NSString *)key;

- (void)setImageWithURLString:(NSString *)urlString;
- (void)setAsTintedWithColor:(UIColor *)color;

@end
