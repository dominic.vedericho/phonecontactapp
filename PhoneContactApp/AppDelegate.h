//
//  AppDelegate.h
//  PhoneContactApp
//
//  Created by Dominic Vedericho on 22/06/19.
//  Copyright © 2019 dominic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactListViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ContactListViewController *contactListViewController;



@end

