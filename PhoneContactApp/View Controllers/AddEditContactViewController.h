//
//  AddEditContactViewController.h
//  PhoneContactApp
//
//  Created by Dominic Vedericho on 22/06/19.
//  Copyright © 2019 dominic. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, AddEditContactViewControllerType) {
    AddEditContactViewControllerTypeAdd,
    AddEditContactViewControllerTypeEdit
};

@protocol AddEditContactViewControllerDelegate <NSObject>

- (void)doneAddOrEditContact;

@end

@interface AddEditContactViewController : BaseViewController

@property (strong, nonatomic) NSString *contactID;
@property (weak, nonatomic) id <AddEditContactViewControllerDelegate> delegate;
@property (nonatomic) AddEditContactViewControllerType addEditContactViewControllerType;

- (void)setAddEditContactViewControllerType:(AddEditContactViewControllerType)addEditContactViewControllerType;

@end

NS_ASSUME_NONNULL_END
