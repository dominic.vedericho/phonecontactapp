//
//  ContactDetailViewController.h
//  PhoneContactApp
//
//  Created by Dominic Vedericho on 22/06/19.
//  Copyright © 2019 dominic. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ContactDetailViewControllerDelegate <NSObject>

- (void)doneAddOrEditDataFromContactDetail;
- (void)doneDeleteContact;

@end

@interface ContactDetailViewController : BaseViewController

@property (weak, nonatomic) id <ContactDetailViewControllerDelegate> delegate;
@property (strong, nonatomic) NSString *contactID;

@end


NS_ASSUME_NONNULL_END
