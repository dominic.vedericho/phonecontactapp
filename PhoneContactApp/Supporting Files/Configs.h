//
//  Configs.h
//  ContactApp
//
//  Created by Dominic Vedericho on 04/12/18.
//  Copyright © 2018 Dominic. All rights reserved.
//

#ifndef Configs_h
#define Configs_h

//Database Table Name
#define DATABASE_TABLE_CONTACT @"ContactRealmModel"

//Color
#define COLOR_GREEN @"50E3C2"

#endif /* Configs_h */
