//
//  DataManager.h
//  PhoneContactApp
//
//  Created by Dominic Vedericho on 22/06/19.
//  Copyright © 2019 dominic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseManager.h"
#import "ContactModel.h"

@interface DataManager : BaseManager

#pragma mark - API Call
+ (void)callAPIGetContactListSuccess:(void (^)(NSArray<ContactModel *> *contactListArray))success
                             failure:(void (^)(NSError *error))failure;
+ (void)callAPIGetContactDetailWithContactID:(NSString *)contactID
                                     success:(void (^)(ContactModel *contactData))success
                                     failure:(void (^)(NSError *error))failure;
+ (void)callAPIAddNewContactWithData:(ContactModel *)contact
                             success:(void (^)(ContactModel *contactData))success
                             failure:(void (^)(NSError *error))failure;
+ (void)callAPIUpdateContactWithContactID:(NSString *)contactID
                              contactData:(ContactModel *)contact
                                  success:(void (^)(ContactModel *contactData))success
                                  failure:(void (^)(NSError *error))failure;
+ (void)callAPIDeleteContactWithContactID:(NSString *)contactID
                                  success:(void (^)(void))success
                                  failure:(void (^)(NSError *error))failure;
+ (void)callAPIMarkAsFavourite:(BOOL)setAsFavourite
                     contactID:(NSString *)contactID
                       success:(void (^)(ContactModel *contactData))success
                       failure:(void (^)(NSError *error))failure;

#pragma mark - Database Call
+ (void)getDatabaseContactListWithSuccess:(void (^)(NSArray *contactListDatabaseArray))success
                                  failure:(void (^)(NSError *error))failure;
+ (void)updateOrInsertContactToDatabaseWithData:(NSArray *)dataArray
                                      tableName:(NSString *)tableName
                                        success:(void (^)(void))success
                                        failure:(void (^)(NSError *error))failure;
+ (void)deleteContactFromDatabaseWithData:(NSArray *)dataArray
                                tableName:(NSString *)tableName
                                  success:(void (^)(void))success
                                  failure:(void (^)(NSError *error))failure;


@end
