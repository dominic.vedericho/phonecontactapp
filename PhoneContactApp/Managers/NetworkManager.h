//
//  NetworkManager.h
//  PhoneContactApp
//
//  Created by Dominic Vedericho on 22/06/19.
//  Copyright © 2019 dominic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseManager.h"

#define NETWORK_MANAGER_NO_CONNECTION_NOTIFICATION_KEY @"Prefs.NetworkManagerNoConnectionNotificationKey"

@interface NetworkManager : BaseManager

+ (NetworkManager *)sharedManager;

- (void)get:(NSString *)urlString
 parameters:(NSMutableDictionary *)parameters
   progress:(void (^)(NSProgress *downloadProgress))progress
    success:(void (^)(NSURLSessionDataTask *dataTask, NSDictionary *responseObject))success
    failure:(void (^)(NSURLSessionDataTask *dataTask, NSError *error))failure;
- (void)post:(NSString *)urlString
  parameters:(NSMutableDictionary *)parameters
    progress:(void (^)(NSProgress *uploadProgress))progress
     success:(void (^)(NSURLSessionDataTask *dataTask, NSDictionary *responseObject))success
     failure:(void (^)(NSURLSessionDataTask *dataTask, NSError *error))failure;
- (void)put:(NSString *)urlString
 parameters:(NSMutableDictionary *)parameters
    success:(void (^)(NSURLSessionDataTask *dataTask, NSDictionary *responseObject))success
    failure:(void (^)(NSURLSessionDataTask *dataTask, NSError *error))failure;
- (void)delete:(NSString *)urlString
parameters:(NSMutableDictionary *)parameters
success:(void (^)(NSURLSessionDataTask *dataTask, NSDictionary *responseObject))success
failure:(void (^)(NSURLSessionDataTask *dataTask, NSError *error))failure;

- (NSString *)urlEncodedStringFromDictionary:(NSDictionary *)parameterDictionary;

@end
