//
//  APIManager.h
//  PhoneContactApp
//
//  Created by Dominic Vedericho on 22/06/19.
//  Copyright © 2019 dominic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseManager.h"

typedef NS_ENUM(NSInteger, APIManagerType) {
    APIManagerTypeContact
};

@interface APIManager : BaseManager

+ (APIManager *)sharedManager;
+ (NSString *)urlForType:(APIManagerType)type;

@end
