//
//  ContactRealmModel.m
//  PhoneContactApp
//
//  Created by Dominic Vedericho on 22/06/19.
//  Copyright © 2019 dominic. All rights reserved.
//

#import "ContactRealmModel.h"

@implementation ContactRealmModel

+ (NSString *)primaryKey {
    NSString *primaryKey = @"contactID";
    return primaryKey;
}

@end
