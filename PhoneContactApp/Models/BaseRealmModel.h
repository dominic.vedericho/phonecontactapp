//
//  BaseRealmModel.h
//  PhoneContactApp
//
//  Created by Dominic Vedericho on 22/06/19.
//  Copyright © 2019 dominic. All rights reserved.
//

#import <Realm/Realm.h>

@interface BaseRealmModel : RLMObject

- (instancetype)initWithDictionary:(NSDictionary *)dict error:(NSError **)err;
- (NSDictionary *)toDictionary;

@end
