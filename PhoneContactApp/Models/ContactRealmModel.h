//
//  ContactRealmModel.h
//  PhoneContactApp
//
//  Created by Dominic Vedericho on 22/06/19.
//  Copyright © 2019 dominic. All rights reserved.
//

#import "BaseRealmModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContactRealmModel : BaseRealmModel

@property (strong, nonatomic) NSString *contactID;
@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *phoneNumber;
@property (strong, nonatomic) NSString *profilePicture;
@property (nonatomic, strong) NSNumber<RLMBool> *isFavorite;
@property (strong, nonatomic) NSString *urlString;

@end

NS_ASSUME_NONNULL_END
