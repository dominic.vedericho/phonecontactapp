//
//  ContactDetailView.m
//  PhoneContactApp
//
//  Created by Dominic Vedericho on 22/06/19.
//  Copyright © 2019 dominic. All rights reserved.
//

#import "ContactDetailView.h"
#import "ContactModel.h"

@interface ContactDetailView ()

@property (strong, nonatomic) UIView *headerView;
@property (strong, nonatomic) CustomImageView *profileImageView;
@property (strong, nonatomic) UILabel *nameLabel;

@property (strong, nonatomic) UIImageView *backImageView;
@property (strong, nonatomic) UILabel *backLabel;

@property (strong, nonatomic) UIImageView *messageImageView;
@property (strong, nonatomic) UILabel *messagePlaceholderLabel;

@property (strong, nonatomic) UIImageView *phoneImageView;
@property (strong, nonatomic) UILabel *phonePlaceholderLabel;

@property (strong, nonatomic) UIImageView *emailImageView;
@property (strong, nonatomic) UILabel *emailPlaceholderLabel;

@property (strong, nonatomic) UIImageView *favouriteImageView;
@property (strong, nonatomic) UILabel *favouritePlaceholderLabel;

@property (strong, nonatomic) UIView *loadingView;
@property (strong, nonatomic) UIActivityIndicatorView *indicatorView;

@end

@implementation ContactDetailView

#pragma mark - Lifecycle
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if(self) {
        self.backgroundColor = [Util getColor:@"F9F9F9"];
        
        _headerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.frame), 335.0f)];
        self.headerView.backgroundColor = [Util getColor:@"F9F9F9"];
        [self addSubview:self.headerView];
        
        UIView *gradientView = [[UIView alloc] initWithFrame:self.headerView.frame];
        CAGradientLayer *gradient = [CAGradientLayer layer];
        
        gradient.frame = gradientView.bounds;
        gradient.colors = @[(id)[UIColor whiteColor].CGColor, (id)[Util getColor:@"E2F7F3"].CGColor, (id)[[Util getColor:@"D9F5EE"] colorWithAlphaComponent:0.1f].CGColor];
        
        [self.headerView.layer insertSublayer:gradient atIndex:0];
        
        _backImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0f, 40.0f, 15.0f, 30.0f)];
        self.backImageView.image = [UIImage imageNamed:@"backArrow"];
        self.backImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.headerView addSubview:self.backImageView];
        
        _backLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.backImageView.frame) + 3.0f, CGRectGetMinY(self.backImageView.frame), 70.0f, 30.0f)];
        self.backLabel.text = NSLocalizedString(@"Contact", @"");
        self.backLabel.textColor = [Util getColor:COLOR_GREEN];
        [self.headerView addSubview:self.backLabel];
        
        _backButton = [[UIButton alloc] initWithFrame:CGRectMake(5.0f, 40.0f, CGRectGetMaxX(self.backLabel.frame) + 5.0f, 30.0f)];
        [self.headerView addSubview:self.backButton];
        
        _editButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.headerView.frame) - 50.0f - 6.0f, 40.0f, 50.0f, 30.0f)];
        [self.editButton setTitle:NSLocalizedString(@"Edit", @"") forState:UIControlStateNormal];
        [self.editButton setTitleColor:[Util getColor:COLOR_GREEN] forState:UIControlStateNormal];
        [self.headerView addSubview:self.editButton];
        
        _profileImageView = [[CustomImageView alloc] initWithFrame:CGRectMake((CGRectGetWidth(self.headerView.frame) - 111.0f) / 2.0f, 84.0f, 111.0f, 111.0f)];
        self.profileImageView.layer.cornerRadius = CGRectGetHeight(self.profileImageView.frame) / 2.0f;
        self.profileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
        self.profileImageView.layer.borderWidth = 3.0f;
        self.profileImageView.clipsToBounds = YES;
        [self.headerView addSubview:self.profileImageView];
        
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(32.0f, CGRectGetMaxY(self.profileImageView.frame) + 8.0f, CGRectGetWidth(self.frame) - 32.0f - 32.0f, 24.0f)];
        self.nameLabel.textAlignment = NSTextAlignmentCenter;
        self.nameLabel.font = [UIFont boldSystemFontOfSize:20.0f];
        [self.headerView addSubview:self.nameLabel];
        
        //Screen size - left gap - right gap - interitem gap
        CGFloat menuItemWidth = (CGRectGetWidth(self.headerView.frame) - 44.0f - 44.0f - (3.0f * 36.0f)) / 4.0f;
        _messageImageView = [[UIImageView alloc] initWithFrame:CGRectMake(44.0f, CGRectGetMaxY(self.nameLabel.frame) + 16.0f, menuItemWidth, menuItemWidth)];
        self.messageImageView.layer.cornerRadius = CGRectGetHeight(self.messageImageView.frame) / 2.0f;
        self.messageImageView.image = [UIImage imageNamed:@"message_button"];
        self.messageImageView.contentMode = UIViewContentModeScaleAspectFit;
        self.messageImageView.clipsToBounds = YES;
        [self.headerView addSubview:self.messageImageView];
        
        _messageButton = [[UIButton alloc] initWithFrame:self.messageImageView.frame];
        [self.headerView addSubview:self.messageButton];
        
        _phoneImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.messageImageView.frame) + 36.0f, CGRectGetMinY(self.messageImageView.frame), menuItemWidth, menuItemWidth)];
        self.phoneImageView.layer.cornerRadius = CGRectGetHeight(self.phoneImageView.frame) / 2.0f;
        self.phoneImageView.image = [UIImage imageNamed:@"call_button"];
        self.phoneImageView.contentMode = UIViewContentModeScaleAspectFit;
        self.phoneImageView.clipsToBounds = YES;
        [self.headerView addSubview:self.phoneImageView];
        
        _phoneButton = [[UIButton alloc] initWithFrame:self.phoneImageView.frame];
        [self.headerView addSubview:self.phoneButton];
        
        _emailImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.phoneImageView.frame) + 36.0f, CGRectGetMinY(self.messageImageView.frame), menuItemWidth, menuItemWidth)];
        self.emailImageView.layer.cornerRadius = CGRectGetHeight(self.emailImageView.frame) / 2.0f;
        self.emailImageView.image = [UIImage imageNamed:@"email_button"];
        self.emailImageView.contentMode = UIViewContentModeScaleAspectFit;
        self.emailImageView.clipsToBounds = YES;
        [self.headerView addSubview:self.emailImageView];
        
        _emailButton = [[UIButton alloc] initWithFrame:self.emailImageView.frame];
        [self.headerView addSubview:self.emailButton];
    
        _favouriteImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.emailImageView.frame) + 36.0f, CGRectGetMinY(self.messageImageView.frame), menuItemWidth, menuItemWidth)];
        self.favouriteImageView.layer.cornerRadius = CGRectGetHeight(self.favouriteImageView.frame) / 2.0f;
        self.favouriteImageView.contentMode = UIViewContentModeScaleAspectFit;
        self.favouriteImageView.clipsToBounds = YES;
        [self.headerView addSubview:self.favouriteImageView];
        
        _favoriteButton = [[UIButton alloc] initWithFrame:self.favouriteImageView.frame];
        [self.headerView addSubview:self.favoriteButton];
        
        //Screen size - left gap - right gap - interitem gap
        CGFloat placeholderLabelWidth = menuItemWidth + 5.0f + 5.0f;
        _messagePlaceholderLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.messageImageView.frame) - 5.0f, CGRectGetMaxY(self.messageImageView.frame) + 6.0f, placeholderLabelWidth, 14.0f)];
        self.messagePlaceholderLabel.text = NSLocalizedString(@"Message", @"");
        self.messagePlaceholderLabel.textAlignment = NSTextAlignmentCenter;
        self.messagePlaceholderLabel.font = [UIFont systemFontOfSize:12.0f];
        self.messagePlaceholderLabel.textColor = [Util getColor:@"4A4A4A"];
        [self.headerView addSubview:self.messagePlaceholderLabel];
        
        _phonePlaceholderLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.messageImageView.frame) + 31.0f, CGRectGetMinY(self.messagePlaceholderLabel.frame), placeholderLabelWidth, 14.0f)];
        self.phonePlaceholderLabel.text = NSLocalizedString(@"Phone", @"");
        self.phonePlaceholderLabel.textAlignment = NSTextAlignmentCenter;
        self.phonePlaceholderLabel.font = [UIFont systemFontOfSize:12.0f];
        self.phonePlaceholderLabel.textColor = [Util getColor:@"4A4A4A"];
        [self.headerView addSubview:self.phonePlaceholderLabel];
        
        _emailPlaceholderLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.phoneImageView.frame) + 31.0f, CGRectGetMinY(self.messagePlaceholderLabel.frame), placeholderLabelWidth, 14.0f)];
        self.emailPlaceholderLabel.text = NSLocalizedString(@"Email", @"");
        self.emailPlaceholderLabel.textAlignment = NSTextAlignmentCenter;
        self.emailPlaceholderLabel.font = [UIFont systemFontOfSize:12.0f];
        self.emailPlaceholderLabel.textColor = [Util getColor:@"4A4A4A"];
        [self.headerView addSubview:self.emailPlaceholderLabel];
        
        _favouritePlaceholderLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.emailImageView.frame) + 31.0f, CGRectGetMinY(self.messagePlaceholderLabel.frame), placeholderLabelWidth, 14.0f)];
        self.favouritePlaceholderLabel.text = NSLocalizedString(@"Favourite", @"");
        self.favouritePlaceholderLabel.textAlignment = NSTextAlignmentCenter;
        self.favouritePlaceholderLabel.font = [UIFont systemFontOfSize:12.0f];
        self.favouritePlaceholderLabel.textColor = [Util getColor:@"4A4A4A"];
        [self.headerView addSubview:self.favouritePlaceholderLabel];
        
        _contactDataTableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0f, CGRectGetMaxY(self.headerView.frame), CGRectGetWidth(self.frame), CGRectGetHeight(self.frame) - CGRectGetHeight(self.headerView.frame)) style:UITableViewStyleGrouped];
        self.contactDataTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.contactDataTableView.backgroundColor = [Util getColor:@"F9F9F9"];
        [self addSubview:self.contactDataTableView];
        
        _loadingView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, CGRectGetWidth([UIScreen mainScreen].bounds), CGRectGetHeight([UIScreen mainScreen].bounds))];
        self.loadingView.backgroundColor = [Util getColor:@"F9F9F9"];
        self.loadingView.alpha = 0.0f;
        [self addSubview:self.loadingView];
        
        _indicatorView = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake((CGRectGetWidth(self.loadingView.frame) - 20.0f) / 2.0f, (CGRectGetHeight(self.loadingView.frame) - 20.0f) / 2.0f, 20.0f, 20.0f)];
        self.indicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        [self.indicatorView startAnimating];
        [self.loadingView addSubview:self.indicatorView];
    }
    
    return self;
}

#pragma mark - Custom Method
- (void)setContactDetailViewWithData:(ContactModel *)contact {
    NSString *profileImage = contact.profilePicture;
    NSString *fullName = [NSString stringWithFormat:@"%@ %@", contact.firstName, contact.lastName];
    BOOL isFavourite = contact.isFavorite;
    
    //Assuming /images/missing.png is no profile image
    if ([profileImage isEqualToString:@""] || [profileImage isEqualToString:@"/images/missing.png"]) {
        //No profile picture
        self.profileImageView.image = [UIImage imageNamed:@"placeholder_photo"];
    }
    else {
        [self.profileImageView setImageWithURLString:profileImage];
    }
    
    self.nameLabel.text = fullName;
    
    if(isFavourite) {
        self.favouriteImageView.image = [UIImage imageNamed:@"favourite_button_selected"];
    }
    else {
        self.favouriteImageView.image = [UIImage imageNamed:@"favourite_button"];
    }
}

- (void)setAsLoading:(BOOL)isLoading animated:(BOOL)isAnimated {
    if (isAnimated) {
        if (isLoading) {
            [UIView animateWithDuration:0.3f animations:^{
                self.loadingView.alpha = 1.0f;
            }];
        }
        else {
            [UIView animateWithDuration:0.3f animations:^{
                self.loadingView.alpha = 0.0f;
            }];
        }
    }
    else {
        if (isLoading) {
            self.loadingView.alpha = 1.0f;
        }
        else {
            self.loadingView.alpha = 0.0f;
        }
    }
}


@end
