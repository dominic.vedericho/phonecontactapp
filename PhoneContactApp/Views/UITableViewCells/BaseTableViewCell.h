//
//  BaseTableViewCell.h
//  PhoneContactApp
//
//  Created by Dominic Vedericho on 22/06/19.
//  Copyright © 2019 dominic. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseTableViewCell : UITableViewCell

@end

NS_ASSUME_NONNULL_END
