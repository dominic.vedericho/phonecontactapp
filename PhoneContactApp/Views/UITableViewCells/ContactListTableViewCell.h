//
//  ContactListTableViewCell.h
//  PhoneContactApp
//
//  Created by Dominic Vedericho on 22/06/19.
//  Copyright © 2019 dominic. All rights reserved.
//

#import "BaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContactListTableViewCell : BaseTableViewCell

- (void)setContactCellWithData:(ContactModel *)contact;

@end

NS_ASSUME_NONNULL_END
