//
//  ContactDetailDeleteTableViewCell.m
//  PhoneContactApp
//
//  Created by Dominic Vedericho on 22/06/19.
//  Copyright © 2019 dominic. All rights reserved.
//

#import "ContactDetailDeleteTableViewCell.h"

@interface ContactDetailDeleteTableViewCell ()

@property (strong, nonatomic) UIView *deleteView;
@property (strong, nonatomic) UILabel *deletePlaceholderLabel;

@end

@implementation ContactDetailDeleteTableViewCell

#pragma mark - Lifecycle
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self) {
        self.contentView.backgroundColor = [Util getColor:@"F9F9F9"];
        
        _deleteView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 20.0f, CGRectGetWidth([UIScreen mainScreen].bounds), 44.0f)];
        self.deleteView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.deleteView];
        
        _deletePlaceholderLabel = [[UILabel alloc] initWithFrame:CGRectMake(24.0f, 0.0f, CGRectGetWidth(self.deleteView.frame) - 24.0f - 24.0f, 44.0f)];
        self.deletePlaceholderLabel.font = [UIFont systemFontOfSize:16.0f];
        self.deletePlaceholderLabel.text = NSLocalizedString(@"Delete", @"");
        self.deletePlaceholderLabel.textColor = [UIColor redColor];
        [self.deleteView addSubview:self.deletePlaceholderLabel];
        
        _deleteButton = [[UIButton alloc] initWithFrame:self.deleteView.frame];
        [self.contentView addSubview:self.deleteButton];
    }
    
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
}

#pragma mark - Custom Method

@end
