//
//  BaseView.h
//  PhoneContactApp
//
//  Created by Dominic Vedericho on 22/06/19.
//  Copyright © 2019 dominic. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseView : UIView

+ (CGRect)frameWithNavigationBar;
+ (CGRect)frameWithoutNavigationBar;

@end

NS_ASSUME_NONNULL_END
