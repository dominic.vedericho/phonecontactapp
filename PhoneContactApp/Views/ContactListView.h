//
//  ContactListView.h
//  PhoneContactApp
//
//  Created by Dominic Vedericho on 22/06/19.
//  Copyright © 2019 dominic. All rights reserved.
//

#import "BaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface ContactListView : BaseView

@property (strong, nonatomic) UITableView *tableView;

- (void)setAsLoading:(BOOL)isLoading animated:(BOOL)isAnimated;

@end

NS_ASSUME_NONNULL_END
